package org.bitbucket.smileman.permissionmanager

import android.content.pm.PackageManager
import org.junit.Assert
import org.junit.Test

/**
 * Created by rou on 30.11.17.
 */

class PermissionManagerTest {
	
	lateinit var permissionManager: PermissionManager
	
	@Test
	fun simpleTest() {
		
		val simplePermissionRequester = object : PermissionRequester {
			override fun requestPermission(permission: String, message: String?) {
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.READ_EXTERNAL_STORAGE),
						intArrayOf(PackageManager.PERMISSION_GRANTED)
				)
			}
			
			override fun checkSelfPermission(permission: String): Boolean {
				TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
			}
		}
		
		val callback = fun(res: Boolean) {
			Assert.assertEquals(true, res)
		}
		
		permissionManager = PermissionManager(simplePermissionRequester)
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback
		)
		
	}
	
	@Test
	fun twoSameRequests() {
		
		val permissionRequester = object : PermissionRequester {
			
			var count = 0
			
			override fun requestPermission(permission: String, message: String?) {
				count += 1
				Assert.assertNotEquals(2, count)
			}
			
			fun ok() {
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.READ_EXTERNAL_STORAGE),
						intArrayOf(PackageManager.PERMISSION_GRANTED)
				)
			}
			
			override fun checkSelfPermission(permission: String): Boolean {
				TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
			}
		}
		
		val callback1 = fun(res: Boolean) {
			Assert.assertEquals(true, res)
		}
		
		val callback2 = fun(res: Boolean) {
			Assert.assertNotEquals(false, res)
		}
		
		permissionManager = PermissionManager(permissionRequester)
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback1
		)
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback2
		)
		
		permissionRequester.ok()
		
		
	}
	
	@Test
	fun twoDifferentRequests() {
		
		val permissionRequester = object : PermissionRequester {
			
			var count = 0
			
			override fun requestPermission(permission: String, message: String?) {
				count += 1
			}
			
			override fun checkSelfPermission(permission: String): Boolean {
				TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
			}
			
			fun ok() {
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.READ_EXTERNAL_STORAGE),
						intArrayOf(PackageManager.PERMISSION_GRANTED)
				)
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.ACCESS_COARSE_LOCATION),
						intArrayOf(PackageManager.PERMISSION_DENIED)
				)
			}
			
		}
		
		val callback1 = fun(res: Boolean) {
			Assert.assertEquals(true, res)
		}
		
		val callback2 = fun(res: Boolean) {
			Assert.assertEquals(false, res)
		}
		
		permissionManager = PermissionManager(permissionRequester)
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback1
		)
		permissionManager.requestPermission(
				PermissionManager.ACCESS_COARSE_LOCATION,
				callback2
		)
		
		permissionRequester.ok()
		Assert.assertEquals(permissionRequester.count, 2)
	}
	
	@Test
	fun twoSameWithInterval() {
		
		val permissionRequester = object : PermissionRequester {
			
			var requests = 0
			
			override fun requestPermission(permission: String, message: String?) {
				requests += 1
			}
			
			
			override fun checkSelfPermission(permission: String): Boolean {
				TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
			}
			
			fun ok1() {
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.READ_EXTERNAL_STORAGE),
						intArrayOf(PackageManager.PERMISSION_DENIED)
				)
			}
			
			fun ok2() {
				permissionManager.onPermissionRequestResult(
						0,
						arrayOf(PermissionManager.READ_EXTERNAL_STORAGE),
						intArrayOf(PackageManager.PERMISSION_GRANTED)
				)
			}
			
		}
		
		var callbackCount = 0
		val callback1 = fun(res: Boolean) {
			Assert.assertEquals(false, res)
			callbackCount += 1
		}
		
		val callback2 = fun(res: Boolean) {
			Assert.assertEquals(true, res)
			callbackCount += 1
		}
		
		permissionManager = PermissionManager(permissionRequester)
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback1
		)
		
		permissionRequester.ok1()
		
		permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				callback2
		)
		
		permissionRequester.ok2()
		
		Assert.assertEquals(2, callbackCount)
		Assert.assertEquals(2, permissionRequester.requests)
	}
}