package org.bitbucket.smileman.permissionmanager

import android.view.View

/**
 * Created by rou on 30.11.17.
 */

interface PermissionRequester {

	fun requestPermission(permission: String, message: String? = null)

	fun checkSelfPermission(permission: String): Boolean
}