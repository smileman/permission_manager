package org.bitbucket.smileman.permissionmanager

import android.Manifest
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View

/**
 * Created by rou on 30.11.17.
 */

class ActivityPermissionRequester(val activity: AppCompatActivity,
                                  val view : View? = null) : PermissionRequester {


	override fun requestPermission(permission: String, message: String?) {
		if (message != null && view != null) {
			requestPermissionRationale(permission, message, view)
		} else {
			ActivityCompat.requestPermissions(activity, arrayOf(permission), 0)
		}
	}

	override fun checkSelfPermission(permission: String): Boolean {
		return (ContextCompat.checkSelfPermission(activity,
				permission)
				!= PackageManager.PERMISSION_GRANTED)
	}

	private fun requestPermissionRationale(permission: String, message: String, view : View) {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
				Manifest.permission.READ_EXTERNAL_STORAGE)) {
			Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
					.setAction("GRANT", {
						ActivityCompat.requestPermissions(activity, arrayOf(permission), 0)
					})
					.show()
		} else {
			ActivityCompat.requestPermissions(activity, arrayOf(permission), 0)
		}
	}


}