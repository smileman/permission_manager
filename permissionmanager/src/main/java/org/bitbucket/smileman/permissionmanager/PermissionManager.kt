package org.bitbucket.smileman.permissionmanager

import android.content.pm.PackageManager
import android.view.View
import java.util.*

/**
 * Created by rou on 30.11.17.
 */

class PermissionManager(private val permissionRequester: PermissionRequester) {

	companion object {

		val READ_EXTERNAL_STORAGE = android.Manifest.permission.READ_EXTERNAL_STORAGE
		val ACCESS_COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION
	}

	private val queue: SortedMap<String, Pair<String?, MutableList<((Boolean) -> Unit)>>> = TreeMap()

	fun requestPermission(permission: String,
	                      callback: ((Boolean) -> Unit),
	                      message: String? = null) {
		if (queue.containsKey(permission)) {
			queue[permission]!!.second.add(callback)
		} else {
			queue.put(permission, Pair(message, mutableListOf(callback)))
			if (queue.size == 1) {
				permissionRequester.requestPermission(permission, message)
			}
		}
	}

	fun onPermissionRequestResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
		queue[permissions[0]]?.second?.forEach {
			it((grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED))
		}
		queue.remove(permissions[0])
		if (!queue.isEmpty()) {
			permissionRequester.requestPermission(queue.firstKey(), queue[queue.firstKey()]?.first)
		}
	}

	fun checkSelfPermission(permission: String): Boolean {
		return permissionRequester.checkSelfPermission(permission)
	}
}