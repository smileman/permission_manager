# Permission Manager v0.0.2 #

Библиотека для получения runtime permissions. 

## История версий ##

* v0.0.2
* Добавлена проверка текущего состояния прав.


## Как подключить ##

#### Переходим на jitpack.io ####
#### Вводим org.bitbucket.smileman/permission_manager и нажимаем lookup ####
#### Выбрать нужный коммит, нажать get it и подключить к проекту ####
* при первой синхронизации gradle запустится сборка на jitpack и будет ошибка (если сборка первый раз)
* проверить на jitpack во вкладке Builds наличие билда и его статус (в логах)
* синхронизировать gradle в проекте еще раз

## Использование ##

1. PermissionManager в конструкторе принимает PermissionRequster, который может возвращать результаты запросов прав. 
   ActivityPermissionRequester - PermissionRequester основанный на активити.
   
```   
... : AppCompatActivity{
...
val permissionManager = PermissionManager(ActivityPermissionRequester(this))
... 
```
   
2. Запрос прав - requestPermission(permission: String, callback: ((Boolean) -> Unit)) 
	permission - константа из PermissionManager. 
```
permissionManager.requestPermission(
				PermissionManager.READ_EXTERNAL_STORAGE,
				{ res ->
					if (res) {
						// права получили
					}
                    else {
                       // пользователь не дал разрешения
                    }
				})
```
	
3. Запрос, в каком состоянии права сейчас - checkSelfPermission(permission: String) : Boolean

```
if (permissionManager.checkSelfPermission(PermissionManager.READ_EXTERNAL_STORAGE)){
    //права уже есть
}
else {
    // можно попробовать запросить
    ... permissionManager.requestPermission ....
}

```

